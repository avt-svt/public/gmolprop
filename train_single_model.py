# hydra
import os
import hydra
import utils.omegaconf_resolvers
from omegaconf import OmegaConf

# project-packages
import utils.utils as utils
import utils.analyze_results as analyze_results
import utils.analyze_mol_data as analyze_mol_data
import source.training as training
import source.inference as inference
from source.models.graph_neural_network import GNN
from source.pyg_molgraph import PyGMolgraphDataset
from source.data_processing import PyGDataModule
from source.applicability_domain_methods.support_vector_machine.ocsvm import SVM_AD 

model_catalog = {
    "GNN": GNN
} 

AD_catalog = AD_catalog = {
    "SVM": SVM_AD
}

@hydra.main(version_base="1.1", config_path="./configs", config_name="Tb")
def single_model_run(cfg):
    # 1. Set ups
    ## Set seed
    utils.seed_everything(cfg.random_seed)
    if cfg.verbose:
        print(f"\nSet seed to: {cfg.random_seed}.")
    ## Setup results dirs
    trained_models = {}
    results = {}
    
    # 2. Data 
    if cfg.verbose:
        print(f"\nStart data preprocessing ...")
    # Sanity check data
    analyze_mol_data.sanity_check_pipeline(
        data_args = cfg.Data
    )

    # Get mol features for graph processing
    mol_features = analyze_mol_data.extract_mol_features(
        data_args = cfg.Data
    )
    
    ## Data to PyG molecular graphs
    dataset = PyGMolgraphDataset(
        root=f"{cfg.Data.path}",
        args=cfg.Data,
        mol_features=mol_features
    )    
    ### optional: usually None
    val_dataset = None
    if cfg.Data.val_path:
        val_dataset = PyGMolgraphDataset(
            root=f"{cfg.Data.val_path}",
            args=cfg.Data,
            mol_features=mol_features
        )
    ### optional: if external test set 
    test_dataset = None
    if cfg.Data.test_path:
        test_dataset = PyGMolgraphDataset(
            root=f"{cfg.Data.test_path}",
            args=cfg.Data,
            mol_features=mol_features
        )
    ## Define data set splits
    datamodule = PyGDataModule(
        config=cfg, 
        dataset=dataset, 
        val_dataset=val_dataset, 
        test_dataset=test_dataset
        )
    ## Hyperparameter settings: Update config with data specific info required for model init (this might depend on the model, TODO)
    cfg = datamodule.update_config_w_data_features(cfg)
    
    # 3. Model
    ## Init model
    model = model_catalog[cfg.Model.type](**dict(cfg.Model))
    if cfg.verbose:
        print(model)
    ## Train model
    model = training.train_model(
        args=cfg,
        model=model,
        datamodule=datamodule
    )
    trained_models["Prediction_model"] = model
    ## Infer model
    model_results = inference.eval_datamodule(
        args=cfg, 
        datamodule=datamodule, 
        model=model
        )
    results["Prediction_results"] = model_results
    ## Save final config
    save_path = f"{os.path.dirname(os.path.abspath(__file__))}/{cfg.save_dir}/config.yaml"
    with open(save_path, "w+") as f:
        OmegaConf.save(cfg, f)

    # 4. Post-training steps
    ## Optional: Train applicability domain
    if cfg.Applicability_domain.method is not None:
        if cfg.Applicability_domain.type == 'post-training':
            # Init AD
            ad_model = AD_catalog[cfg.Applicability_domain.method]()
            # Train AD
            ad_model.train(args=cfg, model=model, datamodule=datamodule)
            trained_models["AD_model"] = model
            # Infer AD
            ad_results = ad_model.infer(args=cfg, model=model, datamodule=datamodule)
            results["AD_results"] = ad_results
        else:
            print(f"Applicability method {cfg.Applicability_domain.method} can not be applied after training.")  
    
    # 5. Save and analyze results
    utils.save_results(
        args=cfg, 
        results=results
        )
    if cfg.Result_analysis.plotting:
        analyze_results.create_plots(
            args=cfg, 
            results=results["Prediction_results"]
            )

    return datamodule, trained_models, results

if __name__ == "__main__":
    single_model_run()
