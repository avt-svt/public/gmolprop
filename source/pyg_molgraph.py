import os.path as osp
import pandas as pd
import torch
from torch_geometric.data import Data, InMemoryDataset
import torch.nn.functional as F
from torch_sparse import coalesce

from tqdm import tqdm
import rdkit
from rdkit import Chem
from rdkit.Chem.rdchem import HybridizationType
from rdkit.Chem.rdchem import BondType as BT

import re

import utils.utils

class MultiGraphData(Data):
    def __init__(self, x_size_dict=None, **kwargs):
        super().__init__(**kwargs)
        self.x_size_dict = x_size_dict

    def __inc__(self, key, value, *args, **kwargs):
        regex = "edge_index_[0-9]+$"
        if re.match(regex, key):
            id = key.split("_")[-1]
            return self.x_size_dict[f"x_{id}"]
        else:
            return super().__inc__(key, value, *args, **kwargs)

class PyGMolgraphDataset(InMemoryDataset):
    r"""PyG dataset with attributed molecular graphs

    Args:
        root (string): Root directory where the dataset should be saved.
    """

    raw_url = ''
    processed_url = ''

    def __init__(
        self, 
        root, 
        transform=None, 
        pre_transform=None,
        pre_filter=None, 
        args=None, 
        mol_features=None,
        without_target=False,
        ):
        self.data_args = args
        self.mol_features = mol_features
        self.used_preprocessed_version = True
        self.without_target = without_target
        super(PyGMolgraphDataset, self).__init__(
            osp.join(osp.dirname(osp.abspath(__file__)), "..", root), 
            transform, 
            pre_transform, 
            pre_filter
            )
        if self.used_preprocessed_version == True:
            print("\n---> Preprocessed version of data set is used. If you want to process again, provide a new data set folder or delete the processed files.")
        self.data, self.slices = torch.load(self.processed_paths[0])

    @property
    def raw_file_names(self):
        return 'raw.pt' if rdkit is None else 'raw.csv'

    @property
    def processed_file_names(self):
        return 'data.pt'

    def download(self):
        pass

    def process(self):
        data_list = self._generate_datalist()
        self.used_preprocessed_version = False
        torch.save(self.collate(data_list), self.processed_paths[0])

    def _generate_datalist(self):
        r"""Molecule graph dataset based on PyG Data object

        returns: 
        list of PyG data objects that correspond to attributed molecular graphs with target property labels
        """

        if rdkit is None:
            raise NotImplementedError(
                'Rdkit not installed. Preprocessing without rdkit not implemented yet.')

        # get dataframe of SMILES and target values
        df = utils.utils.read_data_to_df(path=self.raw_paths[0], data_args=self.data_args, without_target=self.without_target)
        #dir_id = osp.basename(osp.dirname(osp.dirname(self.raw_paths[0])))

        # create PyG Molgraphs for each data point
        data_list = []
        for index, row in tqdm(df.iterrows(), total=df.shape[0]):
            # for multiple graphs in one data point (e.g., mixtures)
            dp_xs, dp_edge_indices, dp_edge_attrs, dp_smiles_mols = [], [], [], []
            for smiles_col in self.data_args.smiles_columns:
                # TODO: should the atom features be individual for each smiles_column?
                smiles = row[smiles_col]
                x, edge_index, edge_attr = self._generate_features(
                    smiles=smiles)
                if x == None:
                    print(
                        f'Warning: Molecule with SMILES {row[smiles_col]} could not be processed! Data point with index {index} is excluded.')
                    break    
                dp_xs.append(x)
                dp_edge_indices.append(edge_index)
                dp_edge_attrs.append(edge_attr)
                dp_smiles_mols.append(smiles)

            graph_features = None
            if self.data_args.extra_input_columns is not None:
                graph_features = torch.tensor(
                    [
                        float(row[add_col]) for add_cols_prop_i in self.data_args.extra_input_columns for add_col in add_cols_prop_i if add_col is not None
                    ]
                ).view(1, -1)
            
            if not self.without_target:
                y = torch.tensor(
                        [
                            float(row[target_col]) for target_col in self.data_args.target_columns
                        ], 
                        dtype=torch.float
                    ).view(1, -1)
            else: 
                y = None

            x_d = {}
            edge_index_d = {}
            edge_attr_d = {}
            smiles_mol_d = {}
            for i, x in enumerate(dp_xs):
                x_d[f"x_{i}"] = x
            for i, e_idx in enumerate(dp_edge_indices):
                edge_index_d[f"edge_index_{i}"] = e_idx 
            for i, e_attr in enumerate(dp_edge_attrs):
                edge_attr_d[f"edge_attr_{i}"] = e_attr 
            for i, s in enumerate(dp_smiles_mols):
                smiles_mol_d[f"smiles_mol_{i}"] = s

            x_size_dict = {k:v.size(0) for k,v in x_d.items()}
            data = MultiGraphData(
                x_size_dict=x_size_dict, 
                y=y,
                graph_features=graph_features,
                idx=f"{index}",
                #idx = f"{dir_id}_{index}",
                **x_d, 
                **edge_index_d, 
                **edge_attr_d, 
                **smiles_mol_d
                )

            data_list.append(data)

        return data_list

    def _generate_features(self, smiles):

        mol = Chem.rdmolfiles.MolFromSmiles(smiles)
        # TODO: check if MolBlock is needed
        # TODO: sanity check of mol-object needed?
        mol_block = Chem.MolToMolBlock(mol)
        mol = Chem.MolFromMolBlock(mol_block)
        if mol is None:
            print('Invalid molecule (None)')
            return None, None, None

        N = mol.GetNumAtoms()
        atom_features = self.mol_features["atom"]
        bond_features = self.mol_features["bond"]

        # atom features
        x = []
        for atom in mol.GetAtoms():
            af = atom_features.calc_feature_one_hot_vec(atom)
            x.append(af)
        x = torch.stack(x)

        # bond features
        row, col, edge_attr = [], [], []
        for bond in mol.GetBonds():
            start, end = bond.GetBeginAtomIdx(), bond.GetEndAtomIdx()
            row += [start, end]
            col += [end, start]
            bf = bond_features.calc_feature_one_hot_vec(bond)
            edge_attr.append(bf)
            edge_attr.append(bf)
        # handle special case of molecule having only one atom (add default edge to prevent message passing error)
        if N == 1:
            # initialize edge features to zero if edges do not exist
            edge_index = torch.tensor([[0], [0]], dtype=torch.long)
            num_edge_features = bond_features.get_total_dim()
            edge_attr = torch.zeros((1, num_edge_features))
        else:
            edge_index = torch.tensor([row, col], dtype=torch.long)
            edge_attr = torch.stack(edge_attr)

        edge_index, edge_attr = coalesce(edge_index, edge_attr, N, N)

        return x, edge_index, edge_attr
