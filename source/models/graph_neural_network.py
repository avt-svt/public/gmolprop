import torch
import torch.nn as nn
from torch.nn import Sequential, Linear, GRU
from torch_scatter import scatter_mean, scatter_add, scatter_max
from torch_geometric.nn import NNConv, GINEConv, Set2Set, GlobalAttention


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

class GNN(nn.Module):

    def __init__(
        self, 
        num_input_smiles=1,
        num_targets=1,
        num_node_features=22, 
        num_edge_features=6, 
        share_embedding=False, 
        conv_type='NNConv',
        num_convs=3,
        use_gru=True,
        dim_fingerprint=64, 
        pool_type='add', 
        activation="LeakyReLU", 
        dropout=0.0,
        task_type='regression',
        multi_channel_MLP=False,
        extra_input_columns=None,
        output_norm_mean=None,
        output_norm_std=None,
        **kwargs
    ):
        
        super().__init__()
        
        print(f"\n---> Start init of GNN model.")

        ### Hyperparameters ###
        # architecture
        self.num_input_smiles = num_input_smiles
        # TODO: share MLP (e.g., act coeff.)
        # TODO: graph interaction
        self.num_targets = num_targets
        self.share_embedding = share_embedding
        self.dim_fingerprint = dim_fingerprint
        self.conv_type = conv_type
        self.num_convs = num_convs
        self.use_gru = use_gru
        self.multi_gru = kwargs.get('multi_gru', False)
        if self.use_gru == False and self.multi_gru: raise ValueError('Error: you try use multiple GRUs but use of GRU is not active.')
        self.pool_type = pool_type
        # MLP-prediction
        self.task_type = task_type
        self.num_clases = None
        if self.task_type == 'classification':
            self.num_classes = kwargs.get('num_classes', 2)
        self.dropout = dropout
        self.multi_channel_MLP = multi_channel_MLP
        self.extra_input_columns = extra_input_columns
        self.output_norm_mean = output_norm_mean
        self.output_norm_std = output_norm_std
        # Activation function
        if activation == "LeakyReLU":
            self.act_func = nn.LeakyReLU
        elif activation == 'ReLU':
            self.act_func = nn.ReLU
        elif activation == 'ELU':
            self.act_func = nn.ELU
        else:
            raise NotImplementedError(f'Activation function {activation} not implemented')
        # data features
        self.num_node_features = num_node_features
        self.num_edge_features = num_edge_features
        
        ### Graph convolutions ###
        # for mixtures without shared embedding, we need one mpnn for each molecule
        num_mpnn_parts = range(1)
        if self.num_input_smiles > 1 and not self.share_embedding:  
            num_mpnn_parts = range(self.num_input_smiles)
            
        # node (and edge) feature dimension adjustment
        self.node_dim_init_lin = nn.ModuleList([torch.nn.Linear(self.num_node_features, self.dim_fingerprint) for _ in num_mpnn_parts])
        self.edge_dim_init_lin = None
        if self.conv_type in ['GINEConv']:
            self.edge_dim_init_lin = nn.ModuleList([torch.nn.Linear(self.num_edge_features, self.dim_fingerprint) for _ in num_mpnn_parts])

        # message passing layers
        self.conv = nn.ModuleList([nn.ModuleList() for _ in num_mpnn_parts])
        self.gru = nn.ModuleList([nn.ModuleList() for _ in num_mpnn_parts])
        for mpnn_idx in num_mpnn_parts:
            for _ in range(self.num_convs):
                nn_conv = self._get_message_passing_layer()
                self.conv[mpnn_idx].append(nn_conv)
                if use_gru:
                    gru = GRU(self.dim_fingerprint, self.dim_fingerprint)
                    self.gru[mpnn_idx].append(gru)
                    # check if multiple GRUs are to be used
                    if not self.multi_gru:
                        break # if False: one GRU for multiple processing steps = num_convs
        
        # special pooling
        dim_channel_in = self.dim_fingerprint # vector dimension after pooling (does not change for add-, mean-, max-pooling)
        if 'set2set' in self.pool_type:
            # set2set layer
            processing_steps = kwargs.get('set2set_processing_steps', 3) # set2set has optional argument of processing steps
            self.set2set = nn.ModuleList([Set2Set(dim_channel_in, processing_steps=processing_steps) for _ in num_mpnn_parts])
            # set2set out
            dim_channel_in = 2*dim_channel_in # vector dimension doubles through set2set pooling
        # TODO: add global attention

        # Multiple graphs per data point
        if self.num_input_smiles > 1:
            dim_channel_in = 2*dim_channel_in # assumes concat TODO: change for different interactions 
            
        # MLP: fingerprint -> property
        self.dropout_layer = nn.Dropout(p=self.dropout)
        num_outputs = self.num_targets
        self.num_mlp_channels = 1
        if self.task_type == 'classification' and self.num_classes != 2:
            num_outputs = self.num_classes
        ## extra input dimensions due to graph features
        self.extra_dims_mlp = []
        if self.extra_input_columns is not None:
            for t_idx in range(self.num_targets):
                self.extra_dims_mlp += [len(self.extra_input_columns[t_idx]) if self.extra_input_columns[t_idx] is not None else 0]
        else:
            self.extra_dims_mlp = [0 for _ in range(self.num_targets)]
        if self.multi_channel_MLP:
            num_outputs = 1
            self.num_mlp_channels = self.num_targets
        else:
            self.extra_dims_mlp = [sum(self.extra_dims_mlp)]
        self.layers_end = torch.nn.ModuleList([nn.Sequential(
            nn.Linear(dim_channel_in + self.extra_dims_mlp[mlp_idx], dim_channel_in),
            self.act_func(),
            self.dropout_layer,
            nn.Linear(dim_channel_in, int(dim_channel_in/2)),
            self.act_func(),
            self.dropout_layer,
            nn.Linear(int(dim_channel_in/2), num_outputs),
        )
        for mlp_idx in range(self.num_mlp_channels)])

        print(f"---> Finished init of GNN model.")

    def forward(self, data, latent_mode=False):
        '''
          Forward pass
        '''

        ### Get data ###
        # for each graph of the data point
        x_dp, edge_index_dp, edge_attr_dp, x_batch_dp = [], [], [], []
        for i in range(self.num_input_smiles):
            x_dp.append(data[f"x_{i}"])
            edge_index_dp.append(data[f"edge_index_{i}"])
            edge_attr_dp.append(data[f"edge_attr_{i}"])
            x_batch_dp.append(data[f"x_{i}_batch"])

        ### Message passing & Pooling ###
        # message passing -> node embeddings
        graph_reprs = []
        mpnn_counter = 0
        for g_idx in range(len(x_dp)):
            # graph i of data point
            x = x_dp[g_idx]
            edge_index = edge_index_dp[g_idx]
            edge_attr = edge_attr_dp[g_idx]
            x_batch = x_batch_dp[g_idx]

            out = self.act_func()(self.node_dim_init_lin[mpnn_counter](x)) 
            if self.edge_dim_init_lin is not None: edge_attr = self.edge_dim_init_lin[mpnn_counter](edge_attr)
            h = out.unsqueeze(0)
            conv_l_idx = 0
            for i in range(self.num_convs):
                out = self.act_func()(self.conv[mpnn_counter][conv_l_idx](out, edge_index, edge_attr)) # convolutional layer
                if self.use_gru: out, h = self.gru[mpnn_counter][conv_l_idx](out.unsqueeze(0), h) # GRU
                out = out.squeeze(0)
                if (self.use_gru and self.multi_gru) or (not self.use_gru): conv_l_idx += 1

            # pooling -> graph embeddings
            x_forward = out # node embeddings of i
            if 'add' in self.pool_type:
                h = scatter_add(x_forward, x_batch, dim=0)
            elif 'mean' in self.pool_type:
                h = scatter_mean(x_forward, x_batch, dim=0)
            elif 'max' in self.pool_type:
                h = scatter_max(x_forward, x_batch, dim=0)[0]
            elif 'set2set' in self.pool_type:
                h = self.set2set[mpnn_counter](x_forward, x_batch)

            if (self.num_input_smiles > 1) and (not self.share_embedding):
                mpnn_counter += 1
                
            graph_reprs.append(h)

        # Graph interaction, TODO: add different interactions
        h = torch.cat(graph_reprs, dim=1)

        # return molecular fingerprints        
        if latent_mode:
            return h

        ### MLP-prediction ###
        y = torch.tensor([]).to(device)
        count_extra_dims = 0
        if self.extra_input_columns is not None:
            extra_graph_features = data.graph_features #.reshape(-1, sum(self.extra_dims_mlp))
        for mlp_channel_idx in range(len(self.layers_end)):
            # Optional: add graph features if available
            mlp_i_extra_dims = self.extra_dims_mlp[mlp_channel_idx]
            if mlp_i_extra_dims != 0:
                h_extended = torch.cat((h, extra_graph_features[:, count_extra_dims:count_extra_dims+mlp_i_extra_dims]), dim=1)
                count_extra_dims += mlp_i_extra_dims
            else: 
                h_extended = h
            y = torch.cat((y, self.layers_end[mlp_channel_idx](h_extended)),dim=1)    
        
        if self.task_type == 'classification':
            if self.num_classes == 2:
                y = torch.sigmoid(y.view(-1)).view(-1, 1)   
            # TODO: not tested
            else: 
                y = torch.nn.Softmax(y.view(-1)).view(-1, 1) 

        return y

    def _get_message_passing_layer(self):
        # Different message passing layers can be found at https://pytorch-geometric.readthedocs.io/en/latest/modules/nn.html#convolutional-layers
        if self.conv_type == 'NNConv':
            edge_nn = Sequential(Linear(self.num_edge_features, 128), nn.ReLU(), Linear(128, self.dim_fingerprint * self.dim_fingerprint))
            return NNConv(self.dim_fingerprint, self.dim_fingerprint, edge_nn, aggr='add')
        elif self.conv_type == 'GINEConv':
            gine_nn = Sequential(Linear(self.dim_fingerprint, int(self.dim_fingerprint*2)), nn.ReLU(), Linear(int(self.dim_fingerprint*2), self.dim_fingerprint))
            return GINEConv(gine_nn, train_eps=True)
        else:
            raise NotImplementedError(f'Conv type {self.conv_type} not implemented')