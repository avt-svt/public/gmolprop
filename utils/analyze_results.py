import os
import sklearn.metrics
import torch
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import json

from . import utils

plt.rcParams.update(plt.rcParamsDefault)


def calculate_regression_metrics(y_true, y_pred, scores=["mae", "mse", "rmse", "mape", "r2", "maxe", "expl_var"]):
        def _get_score(score, y_true, y_pred):
            # calculate metric values
            # Note: Filter data (if data point is not avilable in real data, it should have "inf" as value)
            if score == "mae": 
                return torch.tensor(np.array([sklearn.metrics.mean_absolute_error(y_true[:,i][torch.isfinite(y_true[:,i])], y_pred[:,i][torch.isfinite(y_true[:,i])]) for i in range(y_true.shape[1])]), dtype=torch.float)
            if score == "mse": 
                return torch.tensor(np.array([sklearn.metrics.mean_squared_error(y_true[:,i][torch.isfinite(y_true[:,i])], y_pred[:,i][torch.isfinite(y_true[:,i])]) for i in range(y_true.shape[1])]), dtype=torch.float)
            if score == "rmse": 
                return torch.tensor(np.array([sklearn.metrics.mean_squared_error(y_true[:,i][torch.isfinite(y_true[:,i])], y_pred[:,i][torch.isfinite(y_true[:,i])])**(1/2) for i in range(y_true.shape[1])]), dtype=torch.float)
            if score == "mape": 
                return torch.tensor(np.array([sklearn.metrics.mean_absolute_percentage_error(y_true[:,i][torch.isfinite(y_true[:,i])], y_pred[:,i][torch.isfinite(y_true[:,i])]) for i in range(y_true.shape[1])]), dtype=torch.float)
            if score == "r2": 
                return torch.tensor(np.array([sklearn.metrics.r2_score(y_true[:,i][torch.isfinite(y_true[:,i])], y_pred[:,i][torch.isfinite(y_true[:,i])]) for i in range(y_true.shape[1])]), dtype=torch.float)
            if score == "maxe": 
                return torch.tensor(np.array([sklearn.metrics.max_error(y_true[:,i][torch.isfinite(y_true[:,i])], y_pred[:,i][torch.isfinite(y_true[:,i])]) for i in range(y_true.shape[1])]), dtype=torch.float)
            if score == "expl_var": 
                return torch.tensor(np.array([sklearn.metrics.explained_variance_score(y_true[:,i][torch.isfinite(y_true[:,i])], y_pred[:,i][torch.isfinite(y_true[:,i])]) for i in range(y_true.shape[1])]), dtype=torch.float)


        result_metric = {}
        for s in scores:
            result_metric[s] = _get_score(s, y_true, y_pred)
            
        return result_metric

def calculate_classification_metrics(y_true, y_pred, y_pred_probability=None, scores=["roc_auc", "balanced_accuracy", "confusion_matrix", "sensitivity", "specificity"]):
        def _get_score(score, y_true, y_pred, y_pred_probability):
            # calculate metric values
            # Note: Filter data (if data point is not avilable in real data, it should have "inf" as value)
            if score == "log_loss":
                if y_pred_probability == None:
                    raise ValueError("Predicted class probability is missing. Please provide via 'y_pred_probability' argument.")
                return torch.tensor(np.array([sklearn.metrics.log_loss(y_true[:,i][torch.isfinite(y_true[:,i])], y_pred_probability[:,i][torch.isfinite(y_true[:,i])]) for i in range(y_true.shape[1])]), dtype=torch.float)
            if score == "roc_auc": 
                if y_pred_probability == None:
                    raise ValueError("Predicted class probability is missing. Please provide via 'y_pred_probability' argument.")
                return torch.tensor(np.array([sklearn.metrics.roc_auc_score(y_true[:,i][torch.isfinite(y_true[:,i])], y_pred_probability[:,i][torch.isfinite(y_true[:,i])]) for i in range(y_true.shape[1])]), dtype=torch.float)
            if score == "balanced_accuracy": 
                return torch.tensor(np.array([sklearn.metrics.balanced_accuracy_score(y_true[:,i][torch.isfinite(y_true[:,i])], y_pred[:,i][torch.isfinite(y_true[:,i])]) for i in range(y_true.shape[1])]), dtype=torch.float)
            if score == "confusion_matrix": 
                return torch.tensor(np.array([sklearn.metrics.confusion_matrix(y_true[:,i][torch.isfinite(y_true[:,i])], y_pred[:,i][torch.isfinite(y_true[:,i])]) for i in range(y_true.shape[1])]), dtype=torch.float)
            if score == "sensitivity": 
                #breakpoint()
                sens = torch.tensor([])
                for i in range(y_true.shape[1]):
                    tn, fp, fn, tp = sklearn.metrics.confusion_matrix(y_true[:,i][torch.isfinite(y_true[:,i])], y_pred[:,i][torch.isfinite(y_true[:,i])]).ravel()
                    sens = torch.cat((sens,torch.tensor([tp / (tp + fn)])))
                return sens
            if score == "specificity": 
                spec = torch.tensor([])
                for i in range(y_true.shape[1]):
                    tn, fp, fn, tp = sklearn.metrics.confusion_matrix(y_true[:,i][torch.isfinite(y_true[:,i])], y_pred[:,i][torch.isfinite(y_true[:,i])]).ravel()
                    spec = torch.cat((spec,torch.tensor([tn / (tn + fp)])))
                return spec

        result_metric = {}
        for s in scores:
            result_metric[s] = _get_score(s, y_true, y_pred, y_pred_probability)
            
        return result_metric

def parity_plot(y_true, y_pred, save_path, set_name, property_name, label='', xlim=None, ylim=None, **kwargs):
    
    ape_lines = kwargs.get("ape_lines_percentage", None) # absolute percentage error lines

    axis = plt.subplot()

    plt.xlabel(f'Real {label}')
    plt.ylabel(f'Predicted {label}')

    # when some values are not given, for multi-task setup 
    # TODO: where to handle this
    y_pred = y_pred[~torch.isnan(y_true)]
    y_true = y_true[~torch.isnan(y_true)]

    if xlim is None:
        xlim = [min(min(y_true), min(y_pred)),max(max(y_true), max(y_pred))]
    if ylim is None:
        ylim = xlim

    r2 = sklearn.metrics.r2_score(y_true, y_pred)
    axis.text(0.05, 0.95, f"R2={r2:.2f}", usetex=False, bbox=dict(facecolor='white', edgecolor='black'),
              transform=axis.transAxes)

    axis.scatter(y_true, y_pred, s=5, color='#00549F',  zorder=4)
    axis.plot([xlim[0], xlim[1]], [xlim[0], xlim[1]], color='black',  zorder=3)
    
    if ape_lines is not None:
        axis.plot([xlim[0]+abs(ape_lines*xlim[0]), xlim[1]+abs(xlim[1]*ape_lines)], [xlim[0]+abs(ape_lines*xlim[0]), xlim[1]+abs(xlim[1]*ape_lines)], color='grey', linestyle='--',  zorder=1)
        axis.plot([xlim[0]-abs(ape_lines*xlim[0]), xlim[1]-abs(xlim[1]*ape_lines)], [xlim[0]-abs(ape_lines*xlim[0]), xlim[1]-abs(xlim[1]*ape_lines)], color='grey', linestyle='--',  zorder=2)
    
    axis.set_xlim([xlim[0],xlim[1]])
    axis.set_ylim([ylim[0],ylim[1]])

    save_plot = os.path.join(save_path, f'{set_name}_{property_name.replace("/","-").replace(" ","_")}_parity_plot.png')
    plt.savefig(save_plot, bbox_inches='tight', dpi=1200)
    save_plot_pdf = os.path.join(save_path, f'{set_name}_{property_name.replace("/","-").replace(" ","_")}_parity_plot.pdf')
    plt.savefig(save_plot_pdf, bbox_inches='tight')
    plt.close()

def roc_curve_plot(y_true, y_pred, save_path, set_name, property_name):
    """
    Creates a roc curve plots based on provided data.
    Args:
        y_true: real data
        y_pred: predicted data
    Returns:
        None
    """

    axis = plt.subplot()

    roc_fpr, roc_tpr, thresholds = sklearn.metrics.roc_curve(y_true, y_pred, drop_intermediate=False)
    auc = sklearn.metrics.roc_auc_score(y_true, y_pred)

    axis.plot(roc_fpr, roc_tpr)
    axis.text(0.05, 0.95, f"AUC={auc:.2f}", usetex=False, bbox=dict(facecolor='white', edgecolor='black'),
              transform=axis.transAxes)

    # Print diagonal lines (i.e. BA)
    for i in torch.arange(0, 1, 0.1):
        axis.plot([0, 1 - i], [0 + i, 1], color='gray', linestyle=':', linewidth=1)

    # Print main diagonal
    axis.plot([0, 1], [0, 1], color='black', linestyle='--', linewidth=1)

    plt.legend()
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.xlim(0, 1)
    plt.ylim(0, 1)

    plt.gca().set_aspect('equal', adjustable='datalim')
    plt.grid(True)
    #plt.legend(frameon=False)
    plt.tight_layout()

    secax = plt.twiny()
    secax.set_xlabel(
        'Balanced Accuracy (diagonals)',
        fontsize='small',
        color='gray',
        fontstyle='italic'
    )
    secax.set_adjustable('datalim')
    plt.xlim(1, 0.5)
    secax.set_box_aspect(1)

    secax.set_xticks(torch.arange(1, 0.45, -0.05).tolist())
    xlabels = torch.arange(1, 0.45, -0.05).tolist()
    for j, label in enumerate(xlabels):
        xlabels[j] = '{:.2g}'.format(xlabels[j])
    secax.set_xticklabels(
        xlabels,
        fontdict={
            'fontsize': 'x-small',  # rcParams['axes.titlesize'],
            'color': 'gray',
        },
        rotation=45.0,
        verticalalignment='baseline'
    )

    save_plot = os.path.join(save_path, f'{set_name}_{property_name.replace("/","-").replace(" ","_")}_roc.png')
    plt.savefig(save_plot, bbox_inches='tight', dpi=1200)
    save_plot_pdf = os.path.join(save_path, f'{set_name}_{property_name.replace("/","-").replace(" ","_")}_roc.pdf')
    plt.savefig(save_plot_pdf, bbox_inches='tight')
    plt.close()

def create_plots(args, results, save_dir_results=None):
    print("\n Start plotting ...\n")
    if save_dir_results is None:
        save_dir_results = f"{os.path.dirname(os.path.abspath(__file__))}/../{args.Result_analysis.save_dir_results}"
    os.makedirs(os.path.dirname(save_dir_results), exist_ok=True)
    if args.task_type == 'regression':
        plotting_type = parity_plot
        real_value_name = "real_values"
        prediction_name = "predictions"
    elif args.task_type == 'classification':
        plotting_type = roc_curve_plot
        real_value_name = "real_values"
        prediction_name = "raw_predictions" # note that we need class probabilities for roc_plot
    for result_set, result_set_values in results.items():
        for target_idx, target in enumerate(list(args.Data.targets_to_train)):
            plotting_type(
                y_true=result_set_values[real_value_name][:, target_idx], 
                y_pred=result_set_values[prediction_name][:, target_idx],
                save_path=save_dir_results,
                set_name=result_set,
                property_name=target
                )
            if args.verbose:
                print(f"---> {result_set} results plots created for target property {target}. See {save_dir_results}.")
    
    print("\n... plotting finished.")


def get_average_metric(metric_dict):
    average_metric = {}
    for k, _ in metric_dict[1].items():
        all_values  = [single_metric[k] for _, single_metric in metric_dict.items() if single_metric[k] is not None]
        all_values = torch.stack(all_values)
        average_metric[k] = {
            "mean": all_values.mean(dim=0),
            "std": all_values.std(dim=0),
            "median": all_values.median(dim=0)[0]
        }
    return average_metric
 
