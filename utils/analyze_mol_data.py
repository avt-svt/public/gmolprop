import os.path as osp
import pandas as pd

import rdkit
from rdkit import Chem
from rdkit.Chem.rdchem import HybridizationType
from rdkit.Chem.rdchem import BondType as BT

from utils.Mol_features import MolFeatures, AtomFeatures, BondFeatures
import utils.utils

atom_types = {}

def sanity_check_compounds(smiles_list, id_list=None):

    # check if compounds are unique
    if len(smiles_list) != len(set(smiles_list)):
        raise ValueError(f"Not all compounds are unique.")

    # check if all compounds can be converted to rdkit mol object and back
    mol_list = []
    smiles_kelk_dir = {}
    for s_idx, s in enumerate(smiles_list):
        if not (id_list is None):
            s_id = id_list[s_idx]
        else:
            s_id = "NaN"
        try:
            mol = Chem.MolFromSmiles(s)
            s_kelk = Chem.MolToSmiles(mol)
        except:
            raise ValueError(f"Smiles {s} (C-ID: {s_id}, index: {s_idx}) could not be converted by rdkit.")
        if s_kelk in smiles_kelk_dir.values():
            duplicate_idx = None
            duplicate_smiles = None
            for tmp_s_idx, tmp_s in enumerate(smiles_kelk_dir.values()):
                if s_kelk == tmp_s:
                    duplicate_idx = tmp_s_idx
                    duplicate_smiles = smiles_list[duplicate_idx]
                    if not (id_list is None):
                        duplicate_id = id_list[duplicate_idx]
                    else:
                        duplicate_id = "NaN"
            #raise ValueError(f"Molecule {s} (index: {s_idx}) is duplicate with {duplicate_smiles} (index: {duplicate_idx}).")
            print(f"WARNING: Molecule {s} (C-ID: {s_id}, index: {s_idx}) is duplicate with {duplicate_smiles} (C-ID: {duplicate_id}, index: {duplicate_idx}).")
        
        mol_list.append(mol)
        smiles_kelk_dir[s] = s_kelk
    
    # check again if kelkulized compounds are unique
    if (len(mol_list) != len(set(mol_list))) or (len(smiles_kelk_dir.values()) != len(set(smiles_kelk_dir.values()))):
        if len(set(mol_list)) != len(set(smiles_kelk_dir.values())):
            print(f"WARNING: Number of unique mol objects {len(set(mol_list))} differs from number of unique kelkulized SMILES {len(set(smiles_kelk_dir.values()))}. Note that duplicate SMILES can result in different mol objects.")
        #raise ValueError(f"Not all compounds are unique.")
        print(f"WARNING: Not all compunds are unique. Found {len(smiles_kelk_dir.values()) - len(set(smiles_kelk_dir.values()))} duplicate SMILES.")

    return smiles_kelk_dir

def sanity_check_data(df, input_columns, target_columns, check_conflicts=False):
    '''
    Checks for duplicates
    If duplicates are found: returns pd.Series of duplicates (input combinations that occur in duplicates)
    Otherwise: returns None
    ---
    check_conflicts checks for conflicting target values for duplicate input combinations (same input with different label)
    Note: can take some time if df is large
    '''
    # check uniqueness of data points
    unique_input_series = df.groupby(input_columns).size()
    num_duplicates = df.shape[0] - unique_input_series.shape[0]
    if num_duplicates != 0:
        print(f"WARNING: Data set includes {num_duplicates} duplicate entries (max: {unique_input_series.max()} for {unique_input_series.idxmax()}). Number of unique entries: {unique_input_series.shape[0]} ({unique_input_series.shape[0]/df.shape[0]*100:.2f}%) of total entries: {df.shape[0]}")
        duplicate_input_comb = unique_input_series[unique_input_series>1]
        if (duplicate_input_comb.sum() - duplicate_input_comb.shape[0]) != num_duplicates:
            raise ValueError(f"Something went wrong in catching overlapping entries.")
        # if duplicates: check conflicts in targets, TODO: extend to multi-property
        if check_conflicts:
            unique_input_series_w_labels = df.groupby(input_columns).agg({target_columns[0]: lambda x: len(set(x))})
            conflicting_duplicates = unique_input_series_w_labels[unique_input_series_w_labels[target_columns[0]] > 1]
            if conflicting_duplicates.shape[0] > 0:
                print(f"        -> Found {conflicting_duplicates.shape[0]} conflicting duplicates, i.e., duplicates with different target values (max: {conflicting_duplicates[target_columns[0]].max()} for {conflicting_duplicates[target_columns[0]].idxmax()}).")
        return duplicate_input_comb
    else:
        return None

def cross_check_data(df1, df2, input_columns):
    '''
    Checks for overlaps between data sets
    If overlaps are found: returns pd.Series of overlapping entries between the two data sets
    Otherwise: returns None
    '''
    df1_unique_input_series = df1.groupby(input_columns).size()
    df2_unique_input_series = df2.groupby(input_columns).size()

    df_all = pd.concat([df1, df2], ignore_index=True, sort=False)
    all_unique_input_series = df_all.groupby(input_columns).size()

    # check input overlaps (data leakage)
    num_overlapping_entries = (df1_unique_input_series.shape[0] + df2_unique_input_series.shape[0]) - all_unique_input_series.shape[0] 
    if num_overlapping_entries != 0:
        all_grouped_entries = pd.concat([df1_unique_input_series, df2_unique_input_series]).groupby(input_columns).size()
        overlapping_entries =  all_grouped_entries[all_grouped_entries>1] # all input combinations that occur in both data sets
        if overlapping_entries.shape[0] != num_overlapping_entries:
            raise ValueError(f"Something went wrong in catching overlapping entries.")
        print(f"WARNING: Data sets have {num_overlapping_entries} overlapping entries.")
        return overlapping_entries
    else:
        return None


def sanity_check_pipeline(data_args):

    root_dict = {
        "Train": data_args.path,
        "Val": data_args.val_path, 
        "Test": data_args.test_path
    }
    dfs = []

    ##### Sanity checks ##### 
    print("="*50)
    print("Run data sanity checks")
    decimal_accuracy = 3 # used to round values (catching duplicates due to numerical issues)
    input_cols = utils.utils.data_get_input_cols(data_args=data_args)
    output_cols = utils.utils.data_get_output_cols(data_args=data_args)
    all_smiles = []
    for root_name, root_path in root_dict.items():
        if not root_path:
            continue
        print(f"\nCheck {root_name}")
        data_path = osp.join(osp.dirname(osp.abspath(__file__)), "..", root_path, "raw", "raw.csv")
        df = utils.utils.read_data_to_df(path=data_path, data_args=data_args)

        # Compound list
        print("---> Check compounds ...")
        smiles_list = utils.utils.get_all_smiles_from_df(df=df, smiles_columns=data_args.smiles_columns)
        smiles_list = list(set(smiles_list)) # remove explicit duplicates (sanity_check_data will handle this)
        smiles_kelk_dir = sanity_check_compounds(smiles_list=smiles_list)
        all_smiles += smiles_list
        print("---> ... completed.")

        # Make consistent SMILES by using kelkulized rdkit SMILES
        for sc in data_args.smiles_columns:
            df[sc] = df[sc].map(smiles_kelk_dir)

        print(f"---> Check inputs ...")
        if not (data_args.extra_input_columns is None):
            for extra_inputs in data_args.extra_input_columns:
                for extra_col in extra_inputs:
                    df[extra_col] = df[extra_col].round(decimal_accuracy)
        sanity_result = sanity_check_data(df=df, input_columns=input_cols, target_columns=output_cols)
        if not (sanity_result is None):
            print(sanity_result)
        print("---> ... completed.")


        # Cross-check this data sets and with other data sets
        for o_df_i, o_df in enumerate(dfs):
            print(f"---> Cross-check data set {root_name} and data set {list(root_dict.keys())[o_df_i]} ...")
            sanity_result = cross_check_data(df1=o_df, df2=df, input_columns=input_cols)
        if not (sanity_result is None):
            print(sanity_result)
        print("---> ... completed.")
            
        dfs.append(df)

    # Check all smiles for explicit duplicates
    print("\n---> Check merged data sets ...")
    _ = sanity_check_compounds(list(set(all_smiles)))
    print("---> ... completed.")

    print("="*50)
    
    ##### Statistics ##### 
    """
    # Training data
    for td_idx, td in enumerate(data_dir["train_data"]):
        print(f"Statistic train data {td_idx}")
        get_data_stats(df=td, input_columns=input_columns, target_column=target_column)
        print("="*50)
    # Validation data
    for vd_idx, vd in enumerate(data_dir["val_data"]):
        print(f"Statistic validation data {vd_idx}")
        get_data_stats(df=vd, input_columns=input_columns, target_column=target_column)
        print("="*50)
    """


def get_mol_features(smiles_list, featurization=None):
    # TODO: any other standard features to add?
    # find all atom and bond features occurring in the dataset
    std_af = AtomFeatures()
    std_bf = BondFeatures() 
    if featurization is None:
        featurization = {}
        featurization["atom_features"] = std_af.get_feature_names()
        featurization["bond_features"] = std_bf.get_feature_names()
    data_af =  MolFeatures()
    data_bf = MolFeatures()
    
    # add empty maps for features to be considered
    for af in featurization["atom_features"]:
        f = std_af(af)
        data_af.add_feature(name=f.get_name(), rdkit_func=f.get_rdkit_func())
    for bf in featurization["bond_features"]:
        f = std_bf(bf)
        data_bf.add_feature(name=f.get_name(), rdkit_func=f.get_rdkit_func())

    for smiles in smiles_list:
        mol = Chem.rdmolfiles.MolFromSmiles(smiles)
        mol_block = Chem.MolToMolBlock(mol)
        mol = Chem.MolFromMolBlock(mol_block)
        # add mappings for atom features present in the data
        for atom in mol.GetAtoms():
            for af in data_af.get_all_features():
                tmp_af_value = af.calc_feature(atom)
                if tmp_af_value not in af.get_keys():
                    tmp_mapping_target = std_af(af.get_name()).map(tmp_af_value)
                    af.add_single_mapping(tmp_af_value, tmp_mapping_target)
        # add mappings for bond features present in the data
        for bond in mol.GetBonds():
            for bf in data_bf.get_all_features():
                tmp_bf_value = bf.calc_feature(bond)
                if tmp_bf_value not in bf.get_keys():
                    tmp_mapping_target = std_bf(bf.get_name()).map(tmp_bf_value)
                    bf.add_single_mapping(tmp_bf_value, tmp_mapping_target)
    mol_features = {
        "atom": data_af,
        "bond": data_bf
    }
    return mol_features

def sanity_check_features(feature_mapping):
    for feat in feature_mapping.get_all_features():
        if len(feat.get_keys()) == 0:
            raise ValueError(f"Feature {feat.name} is not present in the data set. Something went wrong.")
        if len(feat.get_keys()) == 1:
            print(f"Warning: Feature {feat.name} has no variation in the data set. All data points have a value of {feat.get_keys()[0]}")
    
def extract_mol_features(data_args):
    root_path_list = [data_args.path, data_args.val_path, data_args.test_path]
    all_mol_features = None
    # get dataframe of SMILES
    for root_idx, root_path in enumerate(root_path_list):
        if not root_path:
            continue
        data_path = osp.join(osp.dirname(osp.abspath(__file__)), "..", root_path, "raw", "raw.csv")
        df = utils.utils.read_data_to_df(path=data_path, data_args=data_args)
        smiles_list = utils.utils.get_all_smiles_from_df(df=df, smiles_columns=data_args.smiles_columns)
        featurization = {
            "atom_features": [af for af in data_args.atom_features],
            "bond_features": [bf for bf in data_args.bond_features]
        }
        # extract features present in data splits
        mol_features = get_mol_features(smiles_list, featurization=featurization)
        if root_idx == 0:
            all_mol_features = mol_features
        else:
            for feat_type in ["atom", "bond"]:
                for feat_name in mol_features[feat_type].get_feature_names():
                    for feat_key, feat_mapping_target in mol_features[feat_type](feat_name).get_mapping().items():
                        if feat_key not in all_mol_features[feat_type](feat_name).get_keys():
                            print(f"Warning: Feature {feat_name} value {feat_key} in data from {root_path} is not in training data from {root_path_list[0]}!")
                            all_mol_features[feat_type](feat_name).add_single_mapping(feat_key, feat_mapping_target)
    
    # sanity check features
    for feat_type in ["atom", "bond"]:
        sanity_check_features(all_mol_features[feat_type])
    
    return all_mol_features
        