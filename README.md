# GMoLprop: Graph Machine Learning for Molecular and Mixture Property Prediction

This is the base repository for the GNN tool for molecular and mixture property prediction developed at AVT.SVT (RWTH Aachen University). The repository is maintained by Jan G. Rittig.

Thank you for using the beta version of this repository! If you have any issues, concerns, or comments, please communicate them using the "Issues" functionality or send an e-mail to jan.rittig@rwth-aachen.de.

![Model_structure](docs/images/GNN_structure.png)

## Usage

This repository contains following folders:

* **configs**: config files
* **data**: directories for various data sets, model results, results analysis
* **doc**: documentation
* **source**: source code
* **utils**: util functions

For **training** the GNN, please install the required environment (see below) and then execute following steps:

* Add your data set in csv-format to `data/03_model_input/<your_data_folder>/Train/raw.csv` (optional: add `/Val/raw.csv` and/or `/Test/raw.csv`).
* Set up your configuration in `configs/<your_config>.yaml` (note that `defaultconfig.yaml` contains default settings). 
* Start training your models with:

`train_single_model.py --config-name <your_config>`
for training a single GNN model, or

`train_ensemble_model.py --config-name <your_config>`
for training an ensemble of GNN models.

For **predicting** with a readily trained GNN(s), execute the following steps:

`predict.py --SMILES <SMILES_string> --model_path <model_path>`
for predicting a molecular property, or

`predict.py --SMILES <SMILES_string> --ensemble_path <ensemble_path>`
for prediciting a molecular property with an ensemble of GNN models.

## Required packages

The code is built upon: 

* **[PyTorch](https://pytorch.org/)**
* **[PyTorch Geometric package](https://github.com/rusty1s/pytorch_geometric)**
* **[RDKit package](https://www.rdkit.org/)**

which need to be installed before using our code.

Additional packages that are required and can be installed via `pip install` rdkit, wandb, hydra-core. Note that rdkit can also be installed via `conda install -c conda-forge rdkit`.

We recommend **setting up a conda environment** using the provided yaml-file:
`env.yml `


## Applications

* Schweidtmann, A. M., Rittig, J. G., König, A., Grohe, M., Mitsos, A., & Dahmen, M. (2020). **[Graph neural networks for prediction of fuel ignition quality](https://doi.org/10.1021/acs.energyfuels.0c01533)**. Energy & fuels, 34(9), 11395-11407.

* Rittig, J. G., Gao, Q., Dahmen, M., Mitsos, A., & Schweidtmann, A. M. (2022). **[Graph neural networks for the prediction of molecular structure-property relationships](https://doi.org/10.48550/arXiv.2208.04852)**. arXiv preprint arXiv:2208.04852.

* Rittig, J. G., Ritzert, M., Schweidtmann, A. M., Winkler, S., Weber, J. M., Morsch, P., Heufer, A., Grohe, M., Mitsos, A, & Dahmen, M. (2023). **[Graph machine learning for design of high‐octane fuels](https://doi.org/10.1002/aic.17971)**. AIChE Journal, 69(4), e17971.

* Rittig, J. G., Hicham, K. B., Schweidtmann, A. M., Dahmen, M., & Mitsos, A. (2023). **[Graph neural networks for temperature-dependent activity coefficient prediction of solutes in ionic liquids](https://doi.org/10.1016/j.compchemeng.2023.108153)**. Computers & Chemical Engineering, 171, 108153.

* Rittig, J. G., Felton, K. C., Lapkin, A. A., & Mitsos, A. (2023). **[Gibbs–Duhem-informed neural networks for binary activity coefficient prediction](https://doi.org/10.1039/D3DD00103B)**. Digital Discovery, 2, 1752-1767.

* Schweidtmann, A. M., Rittig, J. G., Weber, J. M., Grohe, M., Dahmen, M., Leonhard, K., & Mitsos, A. (2023). **[Physical pooling functions in graph neural networks for molecular property prediction](https://doi.org/10.1016/j.compchemeng.2023.108202)**. Computers & Chemical Engineering, 172, 108202.


## How to cite this work

Please cite our works if you use this code:

Overview and tutorial:

```
@misc{Rittig.2022,
 author = {Rittig, Jan G. and Gao, Qinghe and Dahmen, Manuel and Mitsos, Alexander and Schweidtmann, Artur M.},
 title = {Graph neural networks for the prediction of molecular structure-property relationships},
 year = {2022},
 howpublished = {arXiv preprint arXiv:2208.04852},
}
```

Please also refer to the corresponding packages that we use:

Pytorch Geomteric (PyG):

```
@inproceedings{Fey/Lenssen/2019,
  title={Fast Graph Representation Learning with {PyTorch Geometric}},
  author={Fey, Matthias and Lenssen, Jan E.},
  booktitle={ICLR Workshop on Representation Learning on Graphs and Manifolds},
  year={2019},
}
```

RDKit:

```
@misc{rdkit,
 author = {{Greg Landrum}},
 title = {RDKit: Open-Source Cheminformatics},
 url = {http://www.rdkit.org}
}
```
