# RB Data

This folder contains ready biodegradability data for singletask training learning extracted from:

* **[Mansouri et al. (2013)](https://archive.ics.uci.edu/ml/datasets/QSAR+biodegradationm)** - cite

```
@article{doi:10.1021/ci4000213,
author = {Mansouri, Kamel and Ringsted, Tine and Ballabio, Davide and Todeschini, Roberto and Consonni, Viviana},
title = {Quantitative Structure–Activity Relationship Models for Ready Biodegradability of Chemicals},
journal = {Journal of Chemical Information and Modeling},
volume = {53},
number = {4},
pages = {867-878},
year = {2013},
doi = {10.1021/ci4000213},
    note ={PMID: 23469921},

URL = { 
        https://doi.org/10.1021/ci4000213
    
},
eprint = { 
        https://doi.org/10.1021/ci4000213
    
}

}
```

The default data is always used in this program. For other data sources, change the data in the default directory.

